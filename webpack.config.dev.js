var webpack = require('webpack');
var merge = require('webpack-merge');

module.exports = merge(require('./webpack.config.base'),{
  debug: true,
  devtool: 'source-map',
  entry: [
    'webpack-hot-middleware/client'
  ],
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEBUG__: true,
      __PROD__: false
    })
  ]
});
