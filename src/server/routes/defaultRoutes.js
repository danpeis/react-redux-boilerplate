import Boom from 'boom';

var register = function (server, options, next) {
  server.route({
    method: 'GET',
    path: '/static/{param*}',
    config: {
      cache: { expiresIn: 10 * 1000},
      handler: {
        directory: {
          path: alias.path('@public/static')
        }
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/{params*}',
    config: {
      cache: { expiresIn: 60 * 1000},
      handler: (request, reply) => {
        reply.file(alias.path('@public/index.html'))
      }
    }
  });

  next();
};

register.attributes = {
  name: 'defaultRoutes'
};

export default register;