import './defineGlobals';

import Hapi from 'hapi';


if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV === 'development') {
  console.log('Running in development configuration, set NODE_ENV to production to switch');
  global.__DEBUG__ = true;
} else {
  global.__DEBUG__ = false;
}

const server = new Hapi.Server();
server.connection({
  port: 3000
});

server.register([]
  .concat(alias.require('@plugins'))
  .concat(alias.require('@routes')),
  error => {
    if (error) {
      console.error(error);
      throw error;
    }

    server.start(() => {
      console.log('Server running at:', server.info.uri);
    });
  });