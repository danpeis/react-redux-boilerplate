var plugins = [];

if (__DEBUG__)
  plugins = plugins.concat(
    [
      alias.require('@plugins/webpack_hot_reload')
    ]);

plugins = plugins.concat([
  require('inert'),
  require('h2o2'),
  alias.require('@plugins/good')
]);



export default plugins;