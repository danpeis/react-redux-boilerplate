import path from 'path';

var config = alias.require('@root/webpack.config.dev.js');
console.log(config);
const devPlugin = {
  register: require('hapi-webpack-plugin'),
  options: {
    compiler: new require('webpack')(config),
    assets: {
      publicPath: config.output.publicPath,
      stats: {
        colors: true,
        chunks : false,
        hash: false,
        version: false,
        timings: false
      }
    },
    hot: {}
  }
};

export default devPlugin;
