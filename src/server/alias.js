var Alias = require('require-alias');

export default new Alias({
  root: '',
  aliases: {
    '@root': '../..',
    '@public': '../../public',
    '@routes': 'routes',
    '@plugins': 'plugins'
  }
});