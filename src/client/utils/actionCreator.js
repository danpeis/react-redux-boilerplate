function createModuleActionCreator(module) {
  return function(input){
    return createModuleAction(module, input);
  };
}

function createModuleAction(module, input) {
  let function_value;
  if (__DEBUG__) {
    function_value = function(inputValue) {
      var t = require('tcomb');
      let valueCheck_tcomb = input.Validation(t);
      valueCheck_tcomb = valueCheck_tcomb === null || typeof valueCheck_tcomb == 'undefined' ? () => {} : valueCheck_tcomb;
      let tempFailActionCall = t.fail;
      t.fail = message => console.error("[Module action: " + module.ModuleName + "]\n", "Action type: " + input.ActionName + "\n", "Input value: ", inputValue, "\n" + message);
      valueCheck_tcomb(inputValue);
      t.fail = tempFailActionCall;
      return {
        type: module.ModuleName.toUpperCase() + "_" + input.ActionName,
        payload: typeof input.Action ==='undefined' || input.Action === null ? inputValue : input.Action(inputValue)
      };
    };
  }

  if (__PROD__) {
    function_value = function(inputValue) {
      return {
        type: module.ModuleName.toUpperCase() + "_" + input.ActionName,
        payload: typeof input.Action ==='undefined' || input.Action === null ? inputValue : input.Action(inputValue)
      };
    };
  }

  let result = function_value;
  result.type = module.ModuleName.toUpperCase() + "_" + input.ActionName;
  return result;
};

export default {
  createModuleAction,
  createModuleActionCreator
};
