import { createModuleActionCreator } from './actionCreator';
import contains from 'lodash/collection/contains';
import configureStore, { rootReducer } from '../store';

let moduleEntriesContext = require
  .context('../', true, /\.\/modules\/\w+(\/reducers)?\/\w+\.jsx?$/);

let modules = createModules(moduleEntriesContext);
let store = configureStore(modules);
if (__DEBUG__)
if (module.hot)
{
  module.hot.accept(moduleEntriesContext.id, () => {
    console.log('Modules updated');
    moduleEntriesContext = require
      .context('../', true, /\.\/modules\/\w+(\/reducers)?\/\w+\.jsx?$/);
    modules = createModules(moduleEntriesContext);
    store.replaceReducer(rootReducer(modules));
  });
}

function createModules(moduleEntriesContext) {
  return moduleEntriesContext
    .keys()
    .map(moduleEntry => {

      console.log("." + moduleEntry);
      let moduleFolderMatch = moduleEntry.match(/\.\/modules\/(\w+)\/index\.js$/);
      if (typeof moduleFolderMatch === 'undefined' || moduleFolderMatch === null)
        return;

      let moduleFolder = moduleFolderMatch[1];
      let mod = moduleEntriesContext(moduleEntry);

      if(__DEBUG__) {
        var isArray = require('lodash/lang/isArray');
        if (typeof mod.ModuleName !== 'string')
          throw new Error(moduleEntry + "\nModuleName is not properly declared, must be an string.");

        if (typeof mod.Version !== 'string')
          throw new Error(moduleEntry + "\nVersion is not properly declared, must be an string.");
        if (!isArray(mod.Dependencies))
          throw new Error(moduleEntry + "\nDependencies is not properly declared, must be an array.");
        else {
          for (var i = 0; i < mod.Dependencies.length; i++) {
            if (typeof mod.Dependencies[i] !== 'string')
              throw new Error(moduleEntry + "\nDependencies is not properly declared, must be an array of strings or empty array.");
          }
        }
      }

      mod.ActionCreator = createModuleActionCreator(mod);

      let moduleReducer = {};
      let hasReducers = false;
      let moduleReducerRegExp = new RegExp('.\\/modules\\/'+ moduleFolder + '\\/reducers\\/(\\w+)\\.js');

      moduleEntriesContext
        .keys()
        .filter(m => contains(m, './modules/'+ moduleFolder + '/reducers'))
        .map(modRed => {

          hasReducers = true;
          let moduleReducerMatch = modRed.match(moduleReducerRegExp);
          let moduleReducerName = moduleReducerMatch[1];
          moduleReducer[moduleReducerName] = moduleEntriesContext(modRed);
        });

      if (hasReducers)
        mod.ModuleReducer = moduleReducer;
      else
        mod.ModuleReducer = null;

      try {
        mod.ModuleRouter = moduleEntriesContext('./modules/'+ moduleFolder + '/router.jsx');
      } catch (e) {
        console.warn(mod.ModuleName + " module does not have a valid router");
        mod.ModuleRouter = null;
      }
      return mod;
    })
    .filter(mod => typeof mod !== 'undefined');
}

export default {
  modules,
  store,
  id: moduleEntriesContext.id
};
