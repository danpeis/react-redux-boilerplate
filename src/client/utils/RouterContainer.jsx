import modules from './moduleEntriesContext';
import React from 'react';
import { ReduxRouter } from 'redux-router';
import { Route } from 'react-router';

class NoModuleRouter extends React.Component {
  render() {
    return (
      <div>
        <h1 style={({backgroundColor:"#BB0000", color: "#FFFFFF"})}>Any module router declared <br/> please install a module with a <em>router.jsx</em> file</h1>
      </div>
    );
  }
}

class NoMatch extends React.Component {
  render() {
    return (
      <div>
        <h1 style={({backgroundColor:"#BB0000", color: "#FFFFFF"})}>No Match</h1>
      </div>
    );
  }
}

class RouterContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { moduleRouters : modules.modules
      .filter(m => m.ModuleRouter !== null)
      .map(m => (<Route
                          path={m.ModuleRouter.props.path}
                          component={m.ModuleRouter.props.component}
                          children={m.ModuleRouter.props.children}
                          key={m.ModuleName}/>))};

    module.hot.accept(modules.id, () => {
        this.setState({ moduleRouters : modules.modules
          .filter(m => m.ModuleRouter !== null)
          .map(m => (<Route
                              path={m.ModuleRouter.props.path}
                              component={m.ModuleRouter.props.component}
                              children={m.ModuleRouter.props.children}
                              key={m.ModuleName}/>))});
    });
  }

  render() {


    return (
      <ReduxRouter>
        {(() => {
          if (__DEBUG__ && this.state.moduleRouters.length === 0) {
            return [(<Route path="/" key="noModuleRouter" component={NoModuleRouter} />)].concat(<Route
                                path="*"
                                component={NoMatch}
                                key="notFound"/>);;
          } else
            return this.state.moduleRouters.concat(<Route
                                path="*"
                                component={NoMatch}
                                key="notFound"/>);
          })()
        }
      </ReduxRouter>
    );
  }
}

export default RouterContainer;
