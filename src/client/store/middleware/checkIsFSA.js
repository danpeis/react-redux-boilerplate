import { isFSA } from 'flux-standard-action';

export default function logger(store) {
  return next => action => {
    if (!isFSA(action))
      console.error('dispatched action is not a Flux Standard Action');
    return next(action);
  };
}
