import { createStore, applyMiddleware } from 'redux';
import { reduxReactRouter } from 'redux-router';
import { createHistory } from 'history';
import contains from 'lodash/collection/contains';
import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';

let storeCreator;

export function rootReducer(modules) {

  let reducers = {};
  if (modules.length === 0)
    console.error('Any module installed!!! Did you declared the index.js file??');

  modules.map(module => {
    if (__DEBUG__) {
      for(let m in module.Dependencies){
        let moduleName = module.Dependencies[m];
        if (!contains(modules.map(mod => mod.ModuleName), moduleName))
          console.error("'" + module.ModuleName + "' module requires the module dependency '" + moduleName + "'");
      }
    }
    if (typeof module.ModuleName !== 'undefined' && module.ModuleReducer !== null) {
      reducers[module.ModuleName] = combineReducers(module.ModuleReducer);
    } else {
      if (__DEBUG__ && typeof module.ModuleName !== 'undefined')
        console.warn(module.ModuleName + " module does not have a valid reducer");
    }
  });

  reducers["router"] = routerStateReducer;

  return combineReducers(reducers);
}

if (__DEBUG__) {
  var { persistState } = require('redux-devtools');
  var DevTools = require('../utils/DevTools');
  var compose = require('redux').compose;

  const storeCreatorDebug = compose(
    applyMiddleware(
      require('./middleware/logger'),
      require('./middleware/checkIsFSA')
    ),
    reduxReactRouter({
      createHistory
    }),
    DevTools.instrument(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
  )(createStore);
  storeCreator = storeCreatorDebug;

} else {
  const storeCreatorProd = applyMiddleware(
    require('./middleware/logger'),
    require('./middleware/checkIsFSA')
  )(createStore);
  storeCreator = storeCreatorProd;
}

export default function configureStore(modules, initialState) {
  const store = storeCreator(rootReducer(modules), initialState);
  // if (module.hot) {
  //   module.hot.accept(modulesContext.id, () => {
  //     console.log('update rootReducer');
  //     store.replaceReducer(rootReducer());
  //   });
  // }

  return store;
};
