require('react-tap-event-plugin')(); // Remove on react 1.0 release
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import RouterContainer from './utils/RouterContainer';
import {store} from './utils/moduleEntriesContext';

ReactDOM.render(
  <div>
    <Provider store={store}>
      <div>
        <RouterContainer />
        {(() => {
          if (__DEBUG__) {
            var DevTools = require('./utils/DevTools');

            return <DevTools />;
          }
        })()}
      </div>
    </Provider>
  </div>
  , document.getElementById('root'));
