var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {
  entry: [
    './src/client/index.jsx'
  ],
  output: {
    path: path.resolve(__dirname, 'public', 'static'),
    filename: 'app.js',
    publicPath: '/static/'
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      include: path.join(__dirname, 'src'),
      loader: 'babel'
    }]
  },
  resolve: {
    // root: path.resolve(__dirname, 'src', 'client'),
    extensions: ['', '.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
      VERSION: '1.0.0'
    })
  ]
};

module.exports = config;
