var webpack = require('webpack');
var merge = require('webpack-merge');

module.exports = merge(require('./webpack.config.base'), {
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.DefinePlugin({
      __DEBUG__: false,
      __PROD__: true
    })
  ]
});